import { getContext, setContext } from "svelte";

const key = {};

export function getGazefilterContext() {
  return getContext(key);
}

export function setGazefilterContext(module) {
  setContext(key, {
    getGazefilter: () => module,
    getTracker: () => module.tracker,
    getVisualizer: () => module.visualizer,
    getRecorder: () => module.recorder,
  });
}
